# Cahier des charges
## Projet
Nous avons comme projet de concevoir un pac-man que nous appellerons « pacmen ». 
Pac-man est un mini-jeu assez célèbre qui consiste à piloter un personnage qui à 
pour objectif de manger toutes les billes se trouvant dans un labyrinthe, mais 
qui est dérangé par des fantômes. Nous allons essayer de refaire ce jeu de la 
meilleur façon possible, mais aussi à notre manière. 

## Objectif
Nous allons tout faire pour réussir à reproduire le jeu. Pour ceci, nous allons 
reprendre le système de base, mais nous allons enlever les fantômes qui vont 
être remplacé par un « timer » et un système de niveaux. On fera de notre 
possible pour éviter au maximum les bugs qui pourront nuire à notre programme .

## Fonctionnalités et conception
Comme dit précédemment, nous allons reprendre le système de base, avec pacmen 
qui pourra se balader sur le labyrinthe, mais  en enlevant les fantômes (à cause 
du manque de temps) et en ajoutant un timer, accompagné de différents niveaux. 
Si la personne qui pilote, autrement dit l’utilisateur, n’a pas réussi à finir à 
temps le niveau, il aura « perdu » et devra recommencer au niveau où il se 
trouve. Pour réaliser notre jeu, nous nous aiderons du logiciel python pour 
concevoir un tableau, qui sera la structure de notre programme (pour créer le 
labyrinthe). Ce tableau sera composé de différents caractères (« . », « X » et 
« ° ») qui représenterons les différentes fonctionnalités du jeu, qui sont le 
personnage, les murs et les « rues » où il pourra circuler.

# Participants
 - Alexis LELOUP
 - Bastian CORBIN
